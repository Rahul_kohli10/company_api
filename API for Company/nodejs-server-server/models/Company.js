const mongoose = require("mongoose");
const Company = mongoose.Schema({
	name: {
		type: String,
		requied: true
	},
	number: {
		type: String,
		required: true
	},
	email: {
		type: String,
		required: true
	}, 
	gstin: {
		type: String,
		required: true
	},
	about_us: {
		type: String,
		required: true
	},
	address: {
		type: String,
		required: true
	}, 
	logo: {
		type: String,
		required: false 
	}, 
	signature: {
		type: String,
		required: false
	}, 
	business_domain: {
		type: String,
		required: true
	},
	business_type: {
		type: String,
		required: true
	}
})

module.exports = mongoose.model("Company", Company);