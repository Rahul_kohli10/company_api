'use strict';

var fs = require('fs'),
    path = require('path'),
    http = require('http');


var oasTools = require('oas-tools');
var mongoose = require("mongoose");
var app = require('express')();
var swaggerTools = require('oas-tools');
var jsyaml = require('js-yaml');
var serverPort = 8080;

mongoose.connect("mongodb+srv://buzzr:bizzbuzz@cluster0-ein9y.gcp.mongodb.net/test?retryWrites=true&w=majority", {
  useNewUrlParser: true,
  useUnifiedTopology: true
}, () => {
    console.log("MongoDB Connection Established")
})

// swaggerRouter configuration
var options = {
  swaggerUi: path.join(__dirname, '/swagger.json'),
  controllers: path.join(__dirname, './controllers'),
  useStubs: process.env.NODE_ENV === 'development' // Conditionally turn on stubs (mock mode)
};

// The Swagger document (require it, build it programmatically, fetch it from a URL, ...)
var spec = fs.readFileSync(path.join(__dirname,'api/swagger.yaml'), 'utf8');
var swaggerDoc = jsyaml.safeLoad(spec);

var oasDoc = jsyaml.safeLoad(spec);

var options_object = {
  controllers: path.join(__dirname, './controllers'),
  checkControllers: true,
  strict: false,
  router: true,
  validator: true,
  docs: {
    apiDocs: '/api-docs',
    apiDocsPrefix: '',
    swaggerUi: '/docs',
    swaggerUiPrefix: ''
  },
  ignoreUnknownFormats: true
};
 
oasTools.configure(options_object);
// Initialize the Swagger middleware
swaggerTools.initializeMiddleware(swaggerDoc, app, function (middleware) {

  // Interpret Swagger resources and attach metadata to request - must be first in swagger-tools middleware chain
  app.use(middleware.swaggerMetadata());

  // Validate Swagger requests
  app.use(middleware.swaggerValidator());

  // Route validated requests to appropriate controller
  app.use(middleware.swaggerRouter(options));

  // Serve the Swagger documents and Swagger UI
  app.use(middleware.swaggerUi());

  // Start the server

  oasTools.initialize(oasDoc, app, function() {
    http.createServer(app).listen(8080, function() {
      console.log("App up and running!");
    });
  });

});
