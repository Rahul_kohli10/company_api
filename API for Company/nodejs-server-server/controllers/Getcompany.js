const Company = require('../models/Company');

function Getacompany(req, res, next) {
    const uuid = req.swagger.params['uuid'].value;
    Company.findById(uuid, (err, company) => {
        if (err) {
            console.log(err);
        } else {
            res.json(company);
        }
    });
}

module.exports = {
    Getacompany: Getacompany,
};
