'use strict';

var utils = require('../utils/writer.js');
var AddACompany = require('../service/AddACompanyService');

var Company = require("../models/Company")
module.exports.Addacompany = function Addacompany (req, res, next) {
  var company = req.swagger.params['company'].value;
  // AddACompany.addacompany(company)
  //   .then(function (response) {
  //     utils.writeJson(res, response);
  //   })
  //   .catch(function (response) {
  //     utils.writeJson(res, response);
  //   });
  Company.create(company, (err, company) => {
    if (err) { 
      console.log(err)
    } else {
      console.log(company);
      res.json("Company succesfully created")
    }
  })
};
