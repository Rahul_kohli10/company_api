'use strict';

var utils = require('../utils/writer.js');
var StartsANegotiation = require('../service/StartsANegotiationService');

const Negotiation = require("../models/Negotiation")
module.exports.StartNegotiation = function StartNegotiation (req, res, next) {
  var negotiation = req.swagger.params['negotiation'].value;
  // StartsANegotiation.startNegotiation(negotiation)
  //   .then(function (response) {
  //     utils.writeJson(res, response);
  //   })
  //   .catch(function (response) {
  //     utils.writeJson(res, response);
  //   });
  Negotiation.create(negotiation, (err, negotiation) => { 
    if (err) {
      console.log(err);
    } else { 
      console.log(negotiation);
      res.json(negotiation._id);
    }
  })
};

module.exports.GetNegotiation = function GetNegotiation(req, res, next) { 
  const uuid = req.swagger.params['uuid'].value;
  Negotiation.findById(uuid, (err, negotiation) => { 
    if (err) { 
      console.log(err);
    } else {
      res.json(negotiation);
     }
  })
}
