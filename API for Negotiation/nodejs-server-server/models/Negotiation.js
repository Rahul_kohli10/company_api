const mongoose = require("mongoose");
const Negotiation = mongoose.Schema({
	to: {
		type: String,
		required: true
	},
	from: {
		type: String,
		required: true
	},
	last_updated: {
		type: String,
		required: true
	},
	created_date: {
		type: String,
		required: true
	},
	list: [
		{
			items: {
				type: String,
				required: true
			},
			price: {
				type: Number,
				required: true
			},
			quantity: {
				type: Number,
				required: true
			}
		}
	]
});

module.exports = mongoose.model("Negotiation", Negotiation);