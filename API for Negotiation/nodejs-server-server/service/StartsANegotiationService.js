'use strict';


/**
 * Starts a negotiation
 *
 * negotiation Negotiation  (optional)
 * returns successResponse
 **/
exports.startNegotiation = function(negotiation) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "message" : "message"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

